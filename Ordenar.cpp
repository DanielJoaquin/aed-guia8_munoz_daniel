#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "Ordenar.h"

using namespace std;

/*Constructor*/
Ordenar::Ordenar(int N){
  this->N=N;
  /*Inicio Arreglo*/
  Arreglo = new int[N];
  rellenoArreglo();
}

/*Metodos*/
/*Metodo para generar valores en Arreglo*/
void Ordenar::rellenoArreglo(){
  srand(time(NULL));
  /*Recorrido del arreglo*/
  for(int i=0; i<N; i++){
    Arreglo[i] = (rand()%1001) + 1;
  }
}

/*Metodo para realizar copia del Arreglo original*/
void Ordenar::copiaArreglo(int Copia[]){
  for(int i=0; i<N; i++){
    Copia[i] = Arreglo[i];
  }
}

/*Impresion arreglo con Parametros*/
void Ordenar::imprimirArreglo1(int Array[]){
  for(int i=0; i<N; i++){
    cout << " A[" << i <<"]=" << Array[i];
  }
  cout << "\n";
}

/*Imprimir arreglo de la clase*/
void Ordenar::imprimirArreglo2(){
  for(int i=0; i<N; i++){
    cout << " A[" << i <<"]=" << Arreglo[i];
  }
  cout << "\n";
}

/*Transporte elemento mas pequeño a la izq*/
void Ordenar::burbujaMenor(int ArregloBurbujaMenor[]){
  int aux = 0;

  for(int i=1; i<=N; i++){
    for(int j=N-1; j>=i; j--){
      if(ArregloBurbujaMenor[j-1] > ArregloBurbujaMenor[j]){
        aux = ArregloBurbujaMenor[j-1];
        ArregloBurbujaMenor[j-1] = ArregloBurbujaMenor[j];
        ArregloBurbujaMenor[j] = aux;
      }
    }
  }
}

/*Transporte elemento mas pequeño a la der*/
void Ordenar::burbujaMayor(int ArregloBurbujaMayor[]){
  int aux = 0;

  for(int i=N-1; i>=0; i--){
    for(int j=0; j<i; j++){
      if(ArregloBurbujaMayor[j] > ArregloBurbujaMayor[j+1]){
        aux = ArregloBurbujaMayor[j-1];
        ArregloBurbujaMayor[j] = ArregloBurbujaMayor[j+1];
        ArregloBurbujaMayor[j+1] = aux;
      }
    }
  }
}

/*Insercion*/
void Ordenar::insercion(int ArregloInsercion[]){
  int aux = 0;
  int c = 0;

	for(int i=1; i<N; i++){
		aux =  ArregloInsercion[i];
		c = i-1;
		while( (c >= 0) && (aux < ArregloInsercion[c])){
			ArregloInsercion[c] = ArregloInsercion[c];
			c--;
		}
		ArregloInsercion[c+1] = aux;
	}
}

/*Insercion Binaria*/
void Ordenar::insercionBinaria(int ArregloInsercionBinaria[]){
  int aux = 0;
  int izq = 0;
  int der = 0;
  int M = 0;
  int j = 0;

	for(int i = 1; i<N; i++){
		aux = ArregloInsercionBinaria[i];
		izq = 0;
		der = i-1;
		while(izq <= der){
			M = (izq+der)/2;
			if(aux <= ArregloInsercionBinaria[M]){
				der = M-1;
			}
			else{
				izq = M+1;
			}
		}
		j = i-1;
		while(j >= izq){
			ArregloInsercionBinaria[j+1] = ArregloInsercionBinaria[j];
			j--;
		}
		ArregloInsercionBinaria[izq] = aux;
	}
}

/*Seleccion*/
void Ordenar::seleccion(int ArregloSeleccion[]){
  int menor = 0;
  int K = 0;

	for(int i = 0; i<N; i++){
		menor = ArregloSeleccion[i];
		K = i;
		for(int j = i+1; j<N; j++){
			if(ArregloSeleccion[j] < menor){
				menor = ArregloSeleccion[j];
				K = j;
			}
		}
		ArregloSeleccion[K] = ArregloSeleccion[i];
		ArregloSeleccion[i] = menor;
	}
}

/*Shell*/
void Ordenar::shell(int ArregloShell[]){
  int aux = 0;
  int i = 0;
  int entero = N+1;
	bool band;

	while(entero > 1){
		entero = (int)(entero/2);
		band = true;
		while(band == true){
			band = false;
			i = 0;
  	  while((i+entero) < N){
				if(ArregloShell[i] > ArregloShell[i+entero]){
					aux = ArregloShell[i];
					ArregloShell[i] = ArregloShell[i+entero];
					ArregloShell[i+entero] = aux;
					band = true;
				}
				i++;
			}
		}
	}
}

/*Auxiliar QuickSort*/
void Ordenar::reducir(int inicio, int fin, int *&pos){
  int izq = inicio;
	int der = fin;
	int aux = 0;
	bool band = true;
	*pos = inicio;

	while(band == true){
		while(Arreglo[*pos] <= Arreglo[der] && *pos != der){
			der--;
		}
		if(*pos == der){
			band = false;
		}
		else{
			aux = Arreglo[*pos];
			Arreglo[*pos] = Arreglo[der];
			Arreglo[der] = aux;
			*pos = der;
			while(Arreglo[*pos] >= Arreglo[izq] && *pos != izq){
				izq++;
				if(*pos==izq){
					band = false;
				}
				else{
					aux = Arreglo[*pos];
					Arreglo[*pos] = Arreglo[izq];
					Arreglo[izq] = aux;
					*pos = izq;
				}
			}
		}
	}
}

/*QuickSort*/
void Ordenar::quickSort(){
  int tope = 1;
  int inicio = 0;
  int fin = 0;
  int *pos;
	int pilaMenor[N];
  int pilaMayor[N];


	pilaMenor[tope]=-1;
	pilaMayor[tope]=N;

	while(tope > 0){
		inicio = pilaMenor[tope];
		fin = pilaMayor[tope];
		tope--;
		reducir(inicio, fin, pos);
		if(inicio<(*pos-1)){
			tope++;
			pilaMenor[tope]=inicio;
			pilaMayor[tope]=*pos-1;
		}
		if(fin>(*pos+1)){
			tope++;
			pilaMenor[tope]= *pos+1;
			pilaMayor[tope]= fin;
		}
	}
}
