#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "Ordenar.h"

using namespace std;

/*Metodo para el calculo del tiempo*/
int calculoTiempo(int tiempoI, int tiempoF ){
  int TiempoTotal = (tiempoF - tiempoI)/CLOCKS_PER_SEC;
  TiempoTotal = TiempoTotal*1000;
  return TiempoTotal;
}

int main(int argc, char *argv[]){

  system("clear");

  /*Variables*/
  string line = "0";
  int N = 0;
  bool resultados;

  /*Si se ingresan 2 parametros, el programa se ejecuta*/
  if(argc == 3){

    N = atoi(argv[1]);
    line = argv[2];
    if(line.compare("s") == 0){
      resultados = true;
    }
    if(line.compare("n") == 0){
      resultados = false;
    }


    /*Validacion del numero*/
    if(N>1000000){
      cout << "Numero invalido\n";
      cout << "Debe ser menor o igual a 1.000.000\n";
      return 1;
    }
  }
  else{
    cout << "Se necesitan 3 parametros\n";
    return 1;
  }

  /*Declaracion de clase*/
  Ordenar *o = new Ordenar(N);

  /*Declaracion de los arreglos*/
  int ArregloBurbujaMenor[N];
  int ArregloBurbujaMayor[N];
  int ArregloInsercion[N];
  int ArregloInsercionBinaria[N];
  int ArregloSeleccion[N];
  int ArregloShell[N];

  int tiempoI = 0;
  int tiempoF = 0;
  int tiempo = 0;

  /*Copia de arreglos*/
  /*Quiksort ocupa el arreglo de la clase*/
  o->copiaArreglo(ArregloBurbujaMenor);
  o->copiaArreglo(ArregloBurbujaMayor);
  o->copiaArreglo(ArregloInsercion);
  o->copiaArreglo(ArregloInsercionBinaria);
  o->copiaArreglo(ArregloSeleccion);
  o->copiaArreglo(ArregloShell);

  cout << "----------------------------------------------------\n";
  cout << "Metodo\t\t | Tiempo\n";
  cout << "----------------------------------------------------\n";


  /*Ordenamiento de arreglos*/
  /*Burbuja Menor*/
  tiempoI = clock();
  o->burbujaMenor(ArregloBurbujaMenor);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Burbuja Menor\t | " << tiempo << " milisegundos" << endl;

  /*Burbuja Mayor*/
  tiempoI = clock();
  o->burbujaMayor(ArregloBurbujaMayor);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Burbuja Mayor\t | " << tiempo << " milisegundos" << endl;

  /*Metodo Insercion*/
  tiempoI = clock();
  o->insercion(ArregloInsercion);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Insercion\t | " << tiempo << " milisegundos" << endl;

  /*Metodo Insercion Binaria*/
  tiempoI = clock();
  o->insercionBinaria(ArregloInsercionBinaria);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Insercion Binaria| " << tiempo << " milisegundos" << endl;

  /*Metodo Seleccion*/
  tiempoI = clock();
  o->seleccion(ArregloSeleccion);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Seleccion\t | " << tiempo << " milisegundos" << endl;

  /*Metodol Shell*/
  tiempoI = clock();
  o->shell(ArregloShell);
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Shell\t\t | " << tiempo << " milisegundos" << endl;

  /*Metodo QuickSort*/
  /*tiempoI = clock();
  o->quickSort();
  tiempoF = clock();
  tiempo = calculoTiempo(tiempoI, tiempoF);
  cout << "Quicksort\t | " << tiempo << " milisegundos" << endl;
	cout << "----------------------------------------------------" << endl;

  /*Si los resultados es positivo, se muestran arreglos ordenados*/
  if(resultados == true){
    cout << "\nBurbuja Menor\t\t";
    o->imprimirArreglo1(ArregloBurbujaMenor);
    cout << "\nBurbuja Mayor\t\t";
    o->imprimirArreglo1(ArregloBurbujaMayor);
    cout << "\nInsercion\t\t";
    o->imprimirArreglo1(ArregloInsercion);
    cout << "\nINsercion Binaria\t";
    o->imprimirArreglo1(ArregloInsercionBinaria);
    cout << "\nSeleccion\t\t";
    o->imprimirArreglo1(ArregloSeleccion);
    cout << "\nShell\t\t\t";
    o->imprimirArreglo1(ArregloShell);
    /*cout << "Quiksort\t\t";
    o->imprimirArreglo2();*/
  }
  cout << "\n----------------------------------------------------\n";
  cout << "FIN DEL PROGRAMA" << endl;
}
