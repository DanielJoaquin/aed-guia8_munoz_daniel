#include <iostream>
using namespace std;

#ifndef ORDENAR_H
#define ORDENAR_H

class Ordenar{

  private:
    int N=0;
    int *Arreglo;

  public:
    /*Constructor*/
    Ordenar(int N);

    /*Metodos*/
    /*Metodo para generar valores en Arreglo*/
    void rellenoArreglo();

    /*Metodo para realizar copia del Arreglo original*/
    void copiaArreglo(int Copia[]);

    /*Impresion arreglo con Parametros*/
    void imprimirArreglo1(int Array[]);

    /*Imprimir arreglo de la clase*/
    void imprimirArreglo2();

    /*Transporte elemento mas pequeño a la izq*/
    void burbujaMenor(int ArregloBurbujaMenor[]);

    /*Transporte elemento mas pequeño a la der*/
    void burbujaMayor(int ArregloBurbujaMayor[]);

    /*Insercion*/
    void insercion(int ArregloInsercion[]);

    /*Insercion Binaria*/
    void insercionBinaria(int ArregloInsercionBinaria[]);

    /*Seleccion*/
    void seleccion(int ArregloSeleccion[]);

    /*Shell*/
    void shell(int ArregloShell[]);

    /*Auxiliar QuickSort*/
    void reducir(int inicio, int fin, int *&pos);

    /*QuickSort*/
    void quickSort();

};
#endif